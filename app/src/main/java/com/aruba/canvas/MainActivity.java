package com.aruba.canvas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private CanvasViewN cv_test;
    private Button btn_click;
    private Button btn_click2;
    private Button btn_click56;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cv_test = (CanvasViewN) findViewById(R.id.cv_test);

        btn_click = (Button) findViewById(R.id.btn_click);
        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cv_test.addItem(R.mipmap.ic_launcher_round, 1);
            }
        });


        btn_click2 = (Button) findViewById(R.id.btn_click2);
        btn_click2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cv_test.addItem(R.mipmap.ic_launcher_round, 2);
            }
        });

        btn_click56 = (Button) findViewById(R.id.btn_click56);
        btn_click56.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cv_test.addItem(R.mipmap.sofa_2_2_s_r, 2, 5, 6, new CanvasViewN.OnItemClickLinstner() {
                            @Override
                            public void onItemClick(CanvasViewN.Item item, int index) {
                                Tools.showToast(MainActivity.this, "点击了" + index);
                            }

                            @Override
                            public void onPicClick(List<CanvasViewN.Item> items, CanvasViewN.Item item) {

                            }
                        }

                );
            }
        });

    }
}
