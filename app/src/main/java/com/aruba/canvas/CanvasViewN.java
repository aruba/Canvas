package com.aruba.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aruba on 2017/11/23.
 * 设置item到一个画布上
 *
 * @deprecated
 */
public class CanvasViewN extends View {
    private Context context;
    private List<Item> items = new ArrayList<>();
    private Paint mPaint;
    private int touchSlop;
    private Bitmap deleteBitmap;

    public CanvasViewN(Context context) {
        this(context, null);
    }

    public CanvasViewN(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CanvasViewN(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        this.touchSlop = configuration.getScaledTouchSlop();

        pathPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pathPaint.setColor(dashPath_color);
        pathPaint.setStyle(Paint.Style.STROKE);
        pathPaint.setStrokeWidth(3);
        pathPaint.setPathEffect(new DashPathEffect(new float[]{5, 5}, 0));

        deleteBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.delete);
    }

    public void setDeleteResId(int id) {
        deleteBitmap = BitmapFactory.decodeResource(context.getResources(), id);
    }

    private int width;
    private int height;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (width == 0) {
            width = MeasureSpec.getSize(widthMeasureSpec);
            height = MeasureSpec.getSize(heightMeasureSpec);

            width = Math.min(width, height);
            height = width;
        }
        setMeasuredDimension(width, height);
    }


    public interface FinishInflateCallBack {
        public void finishInflate();
    }

    private FinishInflateCallBack finishInflateCallBack;

    public void setFinishInflateCallBack(FinishInflateCallBack finishInflateCallBack) {
        this.finishInflateCallBack = finishInflateCallBack;
    }

    public void addItem(int resId, int weight, @Nullable OnItemClickLinstner onItemClickLinstner) {
        if (rectFs == null || rectFs[0][0] == null) {
            return;
        }

        myRectF[] addMyRectFs = findNotUseRectF(weight);
        Item item = new Item(resId, items.size(), context, addMyRectFs, weight);
        item.resetRectFsT();
        item.onItemClickLinstner = onItemClickLinstner;
        items.add(item);
        postInvalidate();
    }

    public void addItem(int resId, int weight) {
        addItem(resId, weight, null);
    }

    public void addItem(int resId, int weight, int x, int y, @Nullable OnItemClickLinstner onItemClickLinstner) {
        if (rectFs == null || rectFs[0][0] == null) {
            return;
        }

        if (y + weight > rect_countY) {
            return;
        }
        myRectF[] addMyRectFs = new myRectF[weight];
        for (int i = 0; i < addMyRectFs.length; i++) {
            if (!rectFs[x][y + i].isUse) {
                addMyRectFs[i] = rectFs[x][y + i];
            } else {
                return;
            }
        }
        Item item = new Item(resId, items.size(), context, addMyRectFs, weight);
        item.resetRectFsT();
        item.onItemClickLinstner = onItemClickLinstner;
        items.add(item);
        postInvalidate();
    }

    public void addItem(int resId, int weight, int x, int y) {
        addItem(resId, weight, x, y, null);
    }

    /**
     * 找到适合放入的区域
     *
     * @param weight
     * @return
     */
    private myRectF[] findNotUseRectF(int weight) {
        myRectF[] tempMyRectFs = new myRectF[weight];
        boolean isOk = false;
        for (int i = 0; i < rect_countX; i++) {
            for (int j = 0; j < rect_countY; j++) {
                if (j != rect_countY - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                    for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                        if (rectFs[i][j + t] != null && !rectFs[i][j + t].isUse) {
                            tempMyRectFs[t] = rectFs[i][j + t];

                            if (t == weight - 1) {
                                isOk = true;
                            }
                        } else {
                            break;
                        }
                    }

                } else {
                    break;
                }

                if (isOk) {
                    break;
                }

            }

            if (isOk) {
                break;
            }

        }

        if (isOk) {
            return tempMyRectFs;
        } else {
            return null;
        }

    }

    public boolean isShowPic() {
        return showPic;
    }

    public void setShowPic(boolean showPic) {
        this.showPic = showPic;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBackground(canvas);
        if (finishInflateCallBack != null) {
            finishInflateCallBack.finishInflate();
            finishInflateCallBack = null;
        }

        Item item;
        for (int i = 0; i < items.size(); i++) {
            item = items.get(i);

            item.drawRotateItem(canvas, mPaint);
            if (showPic)
                item.drawRotate(canvas, deleteBitmap, mPaint);
        }

    }

    //横着有多少份
    private int rect_countX = 9;
    //竖着有多少份
    private int rect_countY = 9;
    private int rect_padding = 5;
    //虚线颜色
    private int dashPath_color = Color.parseColor("#808080");
    //二维数组存放所有的小区域
    private myRectF[][] rectFs = new myRectF[rect_countX][rect_countY];

    class myRectF {
        public RectF rectF = new RectF();
        public int x;
        public int y;
        public boolean isUse;
    }

    Paint pathPaint;

    /**
     * 画虚线
     *
     * @param c
     */
    private void drawBackground(Canvas c) {
        Path path = new Path();
        float real_height = height - rect_padding * 2;

        float left;
        float top;
        float oldLeft = 0;
        float oldTop = 0;
        for (int i = 0; i < rect_countY + 1; i++) {//画横着的虚线,条数为等份加一

            float y = real_height * i / rect_countY;
            top = y + rect_padding;
            if (i >= 1) {
                //将top和bottom放入数组中
                for (int j = 0; j < rect_countY; j++) {
                    if (rectFs[i - 1][j] == null) {
                        rectFs[i - 1][j] = new myRectF();
                        rectFs[i - 1][j].x = i - 1;
                        rectFs[i - 1][j].y = j;
                    }
                    rectFs[i - 1][j].rectF.top = oldTop;
                    //现在的top即底部
                    rectFs[i - 1][j].rectF.bottom = top;
                }
            }

            //记录当前的top，下次直接放入 区域数组top
            oldTop = top;

            path.reset();
            path.moveTo(rect_padding, top);
            path.lineTo(width - rect_padding, top);

            c.drawPath(path, pathPaint);
        }

        float real_width = width - rect_padding * 2;
        for (int i = 0; i < rect_countX + 1; i++) {//画竖着的虚线
            float x = real_width * i / rect_countX;
            left = x + rect_padding;

            if (i >= 1) {
                for (int j = 0; j < rect_countX; j++) {
                    if (rectFs[j][i - 1] == null) {
                        rectFs[j][i - 1] = new myRectF();
                        rectFs[j][i - 1].x = j;
                        rectFs[j][i - 1].y = i - 1;
                    }
                    rectFs[j][i - 1].rectF.left = oldLeft;
                    rectFs[j][i - 1].rectF.right = left;
                }
            }

            oldLeft = left;

            path.reset();
            path.moveTo(left, rect_padding + rect_padding);
            path.lineTo(left, height - rect_padding);

            c.drawPath(path, pathPaint);
        }
    }

    private float currentX;
    private float currentY;
    private Item currentItem;
    private boolean isLongClick;
    private boolean longClickEnable = true;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                //find item
                currentX = event.getX();
                currentY = event.getY();
                currentItem = findItem();
                if (currentItem != null) {
                    currentItem.resetRectFs();
                    getParent().requestDisallowInterceptTouchEvent(true);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                if (mPreviousUpEvent != null && mCurrentDownEvent != null
                        && isConsideredDoubleTap(mCurrentDownEvent, mPreviousUpEvent, event)) {
                    if (mDoubleTapEnabled) {
                        isDoubleTap = true;
                        if (currentItem != null)
                            currentItem.addDegree();
                    }
                } else {
                    if (currentItem != null && currentItem.onItemClickLinstner != null) {
                        if (showPic && currentItem.clickIndex == -2) {
                            currentItem.onItemClickLinstner.onPicClick(items, currentItem);
                            currentItem.clickIndex = -1;
                        } else if (currentItem.clickIndex != -1) {
                            currentItem.onItemClickLinstner.onItemClick(currentItem, currentItem.clickIndex);
                            currentItem.clickIndex = -1;
                        }
                    }
                }

                mCurrentDownEvent = MotionEvent.obtain(event);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (currentItem == null)
                    break;

                float x2 = event.getX();
                float y2 = event.getY();

                if (longClickEnable == true)
                    isLongClick = isLongPressed(currentX, currentY, x2, y2, event.getDownTime(), event.getEventTime(), 500);

                if (isLongClick) {//长按移动
                    float scrollX = x2 - currentX;
                    float scrollY = y2 - currentY;

                    //移动之后，下个down事件之前，不能再执行longclick
                    longClickEnable = false;

                    currentItem.setPoint(scrollX, scrollY);
                    postInvalidate();

                    currentX = x2;
                    currentY = y2;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (currentItem != null && isLongClick) {
                    myRectF[] newRectFs = isCorrectRegion(currentItem);
                    if (newRectFs != null) {
                        currentItem.resetRectFs();
                        currentItem.myRectFs = newRectFs;
                        currentItem.resetRectFsT();
                    }

                    currentItem.reset();
                    postInvalidate();
                }
                if (currentItem != null && isDoubleTap) {
                    currentItem.resetRectFsT();
                    postInvalidate();
                }
                currentItem = null;
                isLongClick = false;
                isDoubleTap = false;
                longClickEnable = true;

                mPreviousUpEvent = MotionEvent.obtain(event);
                break;
            }
        }

        return true;
    }

    /**
     * 当前item的占用区域是否为正确的区域
     *
     * @param currentItem
     * @return
     */
    private myRectF[] isCorrectRegion(Item currentItem) {

        int weight = currentItem.weight;
        myRectF[] tempMyRectFs = new myRectF[weight];
        boolean isOk = false;
        if (currentItem.direction == 0 || currentItem.direction == 2) {
            for (int i = 0; i < rect_countX; i++) {
                for (int j = 0; j < rect_countY; j++) {
                    if (j != rect_countY - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                        for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                            if (rectFs[i][j + t] != null && !rectFs[i][j + t].isUse) {
                                tempMyRectFs[t] = rectFs[i][j + t];

                                if (t == weight - 1) {
                                    isOk = currentItem.isInRegion(tempMyRectFs);
                                }
                            } else {
                                break;
                            }
                        }

                    } else {
                        break;
                    }

                    if (isOk) {
                        break;
                    }

                }

                if (isOk) {
                    break;
                }

            }

        } else if (currentItem.direction == 1 || currentItem.direction == 3) {
            for (int i = 0; i < rect_countY; i++) {
                for (int j = 0; j < rect_countX; j++) {
                    if (j != rect_countX - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                        for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                            if (rectFs[j + t][i] != null && !rectFs[j + t][i].isUse) {
                                tempMyRectFs[t] = rectFs[j + t][i];

                                if (t == weight - 1) {
                                    isOk = currentItem.isInRegion(tempMyRectFs);
                                }
                            } else {
                                break;
                            }
                        }

                    } else {
                        break;
                    }

                    if (isOk) {
                        break;
                    }

                }

                if (isOk) {
                    break;
                }

            }
        }

        if (isOk) {
            return tempMyRectFs;
        } else {
            return null;
        }

    }

    private final int DOUBLE_TAP_TIMEOUT = 200;
    private MotionEvent mCurrentDownEvent;
    private boolean mDoubleTapEnabled = true;
    private boolean isDoubleTap = true;
    private MotionEvent mPreviousUpEvent;

    /**
     * 是否双击
     *
     * @param firstDown
     * @param firstUp
     * @param secondDown
     * @return
     */
    private boolean isConsideredDoubleTap(MotionEvent firstDown, MotionEvent firstUp, MotionEvent secondDown) {
        if (secondDown.getEventTime() - firstUp.getEventTime() > DOUBLE_TAP_TIMEOUT) {
            return false;
        }
        int deltaX = (int) firstUp.getX() - (int) secondDown.getX();
        int deltaY = (int) firstUp.getY() - (int) secondDown.getY();
        return deltaX * deltaX + deltaY * deltaY < 10000;
    }

    /**
     * * 判断是否有长按动作发生 * @param lastX 按下时X坐标 * @param lastY 按下时Y坐标 *
     *
     * @param thisX         移动时X坐标 *
     * @param thisY         移动时Y坐标 *
     * @param lastDownTime  按下时间 *
     * @param thisEventTime 移动时间 *
     * @param longPressTime 判断长按时间的阀值
     */
    private boolean isLongPressed(float lastX, float lastY, float thisX,
                                  float thisY, long lastDownTime, long thisEventTime,
                                  long longPressTime) {
        float offsetX = Math.abs(thisX - lastX);
        float offsetY = Math.abs(thisY - lastY);
        long intervalTime = thisEventTime - lastDownTime;
        if (offsetX <= touchSlop && offsetY <= touchSlop && intervalTime >= longPressTime) {
            return true;
        }
        return false;
    }

    //添加item时，默认位置的偏移量
    private int deviation = 30;
    //显示删除图片
    private boolean showPic = true;

    private Item findItem() {
        Item item = null;
        //从后找，优先使用最上层item
        for (int i = items.size() - 1; i >= 0; i--) {
            item = items.get(i);
            //当前点击了图片
            if (showPic && currentX < item.myRectFs[item.weight - 1].rectF.right + item.myRectFs[item.weight - 1].rectF.width() / 2 && currentX > item.myRectFs[item.weight - 1].rectF.right - item.myRectFs[item.weight - 1].rectF.width() / 2
                    && currentY > item.myRectFs[item.weight - 1].rectF.top - item.myRectFs[item.weight - 1].rectF.height() / 2 && currentY < item.myRectFs[item.weight - 1].rectF.top + item.myRectFs[item.weight - 1].rectF.height() / 2) {
                item.clickIndex = -2;
                break;
            }

            if (currentX < item.right + deviation && currentX > item.left - deviation
                    && currentY > item.getTop() - deviation && currentY < item.getBottom() + deviation) {

                for (int j = 0; j < item.myRectFs.length; j++) {//获取到当前点击的小区域索引
                    if (currentX < item.myRectFs[j].rectF.right + deviation && currentX > item.myRectFs[j].rectF.left - deviation
                            && currentY > item.myRectFs[j].rectF.top - deviation && currentY < item.myRectFs[j].rectF.bottom + deviation) {
                        item.clickIndex = j;
                        break;
                    }
                }
                break;
            } else {
                item = null;
            }
        }

        return item;
    }


    /**
     * 计算两点与垂直方向夹角
     *
     * @param p1
     * @param p2
     * @return
     */
    public float computeDegree(Point p1, Point p2) {
        float tran_x = p1.x - p2.x;
        float tran_y = p1.y - p2.y;
        float degree = 0.0f;
        float angle = (float) (Math.asin(tran_x / Math.sqrt(tran_x * tran_x + tran_y * tran_y)) * 180 / Math.PI);
        if (!Float.isNaN(angle)) {
            if (tran_x >= 0 && tran_y <= 0) {//第一象限    
                degree = angle;
            } else if (tran_x <= 0 && tran_y <= 0) {//第二象限    
                degree = angle;
            } else if (tran_x <= 0 && tran_y >= 0) {//第三象限    
                degree = -180 - angle;
            } else if (tran_x >= 0 && tran_y >= 0) {//第四象限    
                degree = 180 - angle;
            }
        }
        return degree;
    }

    public class Item {
        private int resId;
        private int id;
        private int weight = 1;
        private float centerX;
        private float centerY;
        private float left;
        private float top;
        private float right;
        private float bottom;
        private float deta_degree;
        private float width;
        private float height;
        private Context context;
        private RectF rectF;
        private myRectF[] myRectFs;
        private int padding = 10;
        //放入的偏移量
        private int in_padding = 20;
        // 0 :朝下 1：朝左 2：朝上 3 ： 朝右
        private int direction = 0;
        private OnItemClickLinstner onItemClickLinstner;
        //默认-1 ， -2：點了圖片
        private int clickIndex = -1;

        public Item(int resId, int id, Context context, myRectF[] addMyRectFs, int weight) {
            this.resId = resId;
            this.id = id;
            this.context = context;
            this.myRectFs = addMyRectFs;
            this.weight = weight;
            this.rectF = new RectF(addMyRectFs[0].rectF.left, addMyRectFs[0].rectF.top,
                    addMyRectFs[weight - 1].rectF.right, addMyRectFs[weight - 1].rectF.bottom);
            //缓存机制
            Bitmap bitmap = getBitmap();
            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;

            width = bitmap.getWidth();
            height = bitmap.getHeight();

            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }

        public void resetRectFs() {
            for (int i = 0; i < myRectFs.length; i++) {
                myRectFs[i].isUse = false;
            }
        }

        public void resetRectFsT() {
            for (int i = 0; i < myRectFs.length; i++) {
                myRectFs[i].isUse = true;
            }
        }

        public void reset() {
            if (direction == 0 || direction == 1) {
                this.rectF = new RectF(myRectFs[0].rectF.left, myRectFs[0].rectF.top,
                        myRectFs[weight - 1].rectF.right, myRectFs[weight - 1].rectF.bottom);
            } else if (direction == 2 || direction == 3) {
                this.rectF = new RectF(myRectFs[weight - 1].rectF.left, myRectFs[weight - 1].rectF.top,
                        myRectFs[0].rectF.right, myRectFs[0].rectF.bottom);
            }


            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;
            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }

        public boolean isInRegion(myRectF[] tempMyRectFs) {
            if (left > tempMyRectFs[0].rectF.left - in_padding
                    && right < tempMyRectFs[weight - 1].rectF.right + in_padding
                    && top > tempMyRectFs[0].rectF.top - in_padding
                    && bottom < tempMyRectFs[weight - 1].rectF.bottom + in_padding) {
                return true;
            }

            return false;
        }

        public int getId() {
            return id;
        }

        public float getLeft() {
            return left;
        }

        public void setLeft(float left) {
            this.left = left;
        }

        public float getTop() {
            return top;
        }

        public void setTop(float top) {
            this.top = top;
        }

        public float getRight() {
            return right;
        }

        public void setRight(float right) {
            this.right = right;
        }

        public float getBottom() {
            return bottom;
        }

        public void setBottom(float bottom) {
            this.bottom = bottom;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public void drawRotate(Canvas canvas, Bitmap rotateBitmap, Paint paint) {
            if (rotateBitmap == null) {
                return;
            }
            canvas.drawBitmap(rotateBitmap, right, top, paint);
        }

        public void drawItem(Canvas canvas, Paint paint) {
            canvas.drawBitmap(getBitmap(), left, top, paint);
        }

        public void drawRotateItem(Canvas canvas, Paint paint) {

            Matrix matrix = new Matrix();
            matrix.postRotate(deta_degree, width / 2, height / 2);
            if (direction == 0 || direction == 2) {
                matrix.postTranslate(left + padding / 2, top + padding / 2);
            } else if (direction == 1 || direction == 3) {
                matrix.postTranslate(left - rectF.height() / (2 * weight) + padding / 2, top + rectF.height() / (weight * 2) + padding / 2);
            }

            canvas.drawBitmap(getBitmap(), matrix, paint);
        }

        private float oldDegree;

        /**
         * 通过此方法来控制旋转度数，如果超过360，让它求余，防止，该值过大造成越界
         *
         * @param added
         */
        private void addDegree(float added) {
            if (oldDegree == 0) {
                oldDegree = added;
            }

            deta_degree += added - oldDegree;
            if (deta_degree > 360 || deta_degree < -360) {
                deta_degree = deta_degree % 360;
            }

            oldDegree = added;
        }

        /**
         * 每次+90度
         */
        private void addDegree() {
            deta_degree += 90;
            if (deta_degree > 360 || deta_degree < -360) {
                deta_degree = deta_degree % 360;
            }

            if (weight == 1) {
                return;
            }

            if (deta_degree == 90) {//顺时针转90
                direction = 1;
            } else if (deta_degree == 180) {
                direction = 2;
            } else if (deta_degree == 270) {
                direction = 3;
            } else if (deta_degree == 360 || deta_degree == 0) {
                direction = 0;
            }

            myRectF[] tempRectFs = new myRectF[weight];
            tempRectFs[0] = myRectFs[0];
            try {
                for (int i = 1; i < myRectFs.length; i++) {//第一个元素不变
                    if (direction == 1) {//顺时针转90,x变为第一个元素x+i，y变为第一个元素y
                        tempRectFs[i] = rectFs[myRectFs[0].x + i][myRectFs[0].y];
                        if (tempRectFs[i].isUse) {
                            throw new Exception();
                        }
                        tempRectFs[i].isUse = true;
                    } else if (direction == 2) {//顺时针转90,x变为第一个元素x，y变为第一个元素y-i
                        tempRectFs[i] = rectFs[myRectFs[0].x][myRectFs[0].y - 1];
                        if (tempRectFs[i].isUse) {
                            throw new Exception();
                        }
                        tempRectFs[i].isUse = true;
                    } else if (direction == 3) {//顺时针转90,x变为第一个元素x -i，y变为第一个元素y
                        tempRectFs[i] = rectFs[myRectFs[0].x - 1][myRectFs[0].y];
                        if (tempRectFs[i].isUse) {
                            throw new Exception();
                        }
                        tempRectFs[i].isUse = true;
                    } else if (direction == 0) {//顺时针转90,x变为第一个元素x，y变为第一个元素y+i
                        tempRectFs[i] = rectFs[myRectFs[0].x][myRectFs[0].y + i];
                        if (tempRectFs[i].isUse) {
                            throw new Exception();
                        }
                        tempRectFs[i].isUse = true;
                    }
                }

                resetRectFs();
                myRectFs = tempRectFs;
                reset();
            } catch (Exception e) {
                e.printStackTrace();
                //回滚
                deta_degree -= 90;
                direction--;
                if (direction < 0) {
                    direction = 3;
                }
                if (deta_degree > 360 || deta_degree < -360) {
                    deta_degree = deta_degree % 360;
                }
            }

        }

        public Bitmap getBitmap() {
            if (context == null) {
                return null;
            }

            //缓存机制中取
            if (ImageCache.getInstance().getBitmapFromMemCache(String.valueOf(id)) == null) {
                //缓存机制
                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resId, new BitmapFactory.Options());
                Matrix matrix = new Matrix();
                matrix.postScale((rectF.width() - padding) / (float) bitmap.getWidth(), (rectF.height() - padding) / (float) bitmap.getHeight());
                //长和宽放大缩小的比例
                Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ImageCache.getInstance().addBitmapToMemoryCache(String.valueOf(id),
                        resizeBmp);
                bitmap.recycle();
            }

            return ImageCache.getInstance().getBitmapFromMemCache(String.valueOf(id));
        }

        public void setPoint(float scrollX, float scrollY) {
            left += scrollX;
            right += scrollX;
            top += scrollY;
            bottom += scrollY;

            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }
    }

    public interface OnItemClickLinstner {
        public void onItemClick(Item item, int index);

        public void onPicClick(List<Item> items, Item item);
    }
}
