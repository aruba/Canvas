package com.aruba.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aruba on 2017/11/23.
 * 设置item到一个画布上
 */
public class CanvasView extends View {
    private Context context;
    public List<Item> items = new ArrayList<>();
    private Paint mPaint;
    private int touchSlop;
    private int deleteDrawableResId = R.mipmap.delete;

    public CanvasView(Context context) {
        this(context, null);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        this.touchSlop = configuration.getScaledTouchSlop();

        pathPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pathPaint.setColor(dashPath_color);
        pathPaint.setStyle(Paint.Style.STROKE);
        pathPaint.setStrokeWidth(3);
        pathPaint.setPathEffect(new DashPathEffect(new float[]{5, 5}, 0));
    }

    private Bitmap getDeleteBitmap(int resId) {
        if (resId == 0) {
            resId = deleteDrawableResId;
        }
        Bitmap bitmap = ImageCache.getInstance().getBitmapFromMemCache(resId + "");
        if (bitmap == null) {
            bitmap = BitmapFactory.decodeResource(context.getResources(), resId);
            ImageCache.getInstance().addBitmapToMemoryCache(resId + "", bitmap);
        }
        return bitmap;
    }

    public void setDeleteResId(int id) {
        deleteDrawableResId = id;
    }

    private int width;
    private int height;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
//        if (height != 0) {
//            width = Math.min(width, height);
//        }

        height = (int) (width * rect_countX / (float) rect_countY);
        isSizeChanged = true;
        setMeasuredDimension(width, height);
    }


    public interface FinishInflateCallBack {
        void finishInflate();
    }

    private FinishInflateCallBack finishInflateCallBack;

    public void setFinishInflateCallBack(FinishInflateCallBack finishInflateCallBack) {
        this.finishInflateCallBack = finishInflateCallBack;
    }

    public void addItem(int resId, Object id, int weight, int direction, boolean showDelete, @Nullable OnItemClickLinstner onItemClickLinstner) {
        if (rectFs == null || rectFs[0][0] == null) {
            return;
        }

        myRectF[] addMyRectFs = findNotUseRectF(weight);
        if (addMyRectFs == null) {
            return;
        }

        createItem(resId, id, weight, direction, addMyRectFs, showDelete, onItemClickLinstner);
    }

    public void addItemReverse(int resId, Object tag, int weight, int direction, boolean showDelete, @Nullable OnItemClickLinstner onItemClickLinstner) {
        if (rectFs == null || rectFs[0][0] == null) {
            return;
        }

        myRectF[] addMyRectFs = findNotUseRectFReverse(weight);
        if (addMyRectFs == null) {
            return;
        }

        createItem(resId, tag, weight, direction, addMyRectFs, showDelete, onItemClickLinstner);
    }

    public void addItem(int resId, int weight) {
        addItem(resId, -1, weight, 0, true, null);
    }

    public void addItem(int resId, int weight, @Nullable OnItemClickLinstner onItemClickLinstner) {
        addItem(resId, -1, weight, 0, true, onItemClickLinstner);
    }

    public void addItem(int resId, Object id, int weight, @Nullable OnItemClickLinstner onItemClickLinstner) {
        addItem(resId, id, weight, 0, true, onItemClickLinstner);
    }

    public void addItem(int resId, Object id, int weight, boolean showDelete, @Nullable OnItemClickLinstner onItemClickLinstner) {
        addItem(resId, id, weight, 0, showDelete, onItemClickLinstner);
    }

    public void addItem(int resId, Object id, int weight, int x, int y, int direction, boolean showDelete, @Nullable OnItemClickLinstner onItemClickLinstner) {
        if (rectFs == null || rectFs[0][0] == null) {
            return;
        }

        if (x > rect_countX || y > rect_countY) {
            return;
        }

        if (direction == 0 && y + weight > rect_countY) {
            return;
        }
        myRectF[] addMyRectFs = new myRectF[weight];
        try {
            if (weight == 2) {
                if (direction == 0) {// --
                    if (rectFs[x][y].isUse || rectFs[x][y + 1].isUse) {
                        return;
                    }
                } else if (direction == 1) {// |
                    if (rectFs[x][y].isUse || rectFs[x + 1][y].isUse) {
                        return;
                    }
                } else if (direction == 2) {// --//适配ios
//                    if (rectFs[x][y - 1].isUse || rectFs[x][y].isUse) {
//                        return;
//                    }
                    if (rectFs[x][y].isUse || rectFs[x][y + 1].isUse) {
                        return;
                    }
                } else if (direction == 3) {// |//适配ios
//                    if (rectFs[x - 1][y].isUse || rectFs[x][y].isUse) {
//                        return;
//                    }
                    if (rectFs[x][y].isUse || rectFs[x + 1][y].isUse) {
                        return;
                    }
                }
            } else if (weight == 1) {
                if (rectFs[x][y].isUse) {
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (weight == 2) {
            //适配ios
            if (direction == 2) {
                for (int i = 0; i < addMyRectFs.length; i++) {
                    addMyRectFs[i] = rectFs[x][y + i + 1];
                }
            } else if (direction == 3) {//适配ios
                for (int i = 0; i < addMyRectFs.length; i++) {
                    addMyRectFs[i] = rectFs[x + 1][y + i];
                }
            } else {
                for (int i = 0; i < addMyRectFs.length; i++) {
                    addMyRectFs[i] = rectFs[x][y + i];
                }
            }
        } else {
            for (int i = 0; i < addMyRectFs.length; i++) {
                addMyRectFs[i] = rectFs[x][y + i];
            }
        }

        createItem(resId, id, weight, direction, addMyRectFs, showDelete, onItemClickLinstner);
    }

    private void createItem(int resId, Object id, int weight, int direction, myRectF[] addMyRectFs, boolean showDelete, @Nullable OnItemClickLinstner onItemClickLinstner) {
        Item item = new Item(resId, id, addMyRectFs, weight);
        if (direction != 0)
            item.setDirection(direction);
        item.resetRectFsT();
        item.onItemClickLinstner = onItemClickLinstner;
        if (!showDelete) {
            item.setShowPic(false);
        }
        items.add(item);
        postInvalidate();
    }

    public void addItem(int resId, int weight, int x, int y, int direction) {
        addItem(resId, -1, weight, x, y, direction, true, null);
    }

    public void addItem(int resId, int weight, int x, int y) {
        addItem(resId, -1, weight, x, y, 0, true, null);
    }

    public void reset() {
        if (items.size() == 0) {
            return;
        }

        for (int i = 0; i < items.size(); i++) {
            items.get(i).resetRectFs();
        }

        items.clear();
    }

    /**
     * 找到适合放入的区域
     *
     * @param weight
     * @return
     */
    private myRectF[] findNotUseRectF(int weight) {
        myRectF[] tempMyRectFs = new myRectF[weight];
        boolean isOk = false;
        for (int i = 0; i < rect_countX; i++) {
            for (int j = 0; j < rect_countY; j++) {
                if (j != rect_countY - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                    for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                        if (rectFs[i][j + t] != null && !rectFs[i][j + t].isUse) {
                            tempMyRectFs[t] = rectFs[i][j + t];

                            if (t == weight - 1) {
                                isOk = true;
                            }
                        } else {
                            break;
                        }
                    }

                } else {
                    break;
                }

                if (isOk) {
                    break;
                }

            }

            if (isOk) {
                break;
            }

        }

        if (isOk) {
            return tempMyRectFs;
        } else {
            return null;
        }

    }

    private myRectF[] findNotUseRectFReverse(int weight) {
        myRectF[] tempMyRectFs = new myRectF[weight];
        boolean isOk = false;
        for (int i = rect_countX - 1; i > 0; i--) {
            for (int j = 0; j < rect_countY; j++) {
                if (j != rect_countY - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                    for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                        if (rectFs[i][j + t] != null && !rectFs[i][j + t].isUse) {
                            tempMyRectFs[t] = rectFs[i][j + t];

                            if (t == weight - 1) {
                                isOk = true;
                            }
                        } else {
                            break;
                        }
                    }

                } else {
                    break;
                }

                if (isOk) {
                    break;
                }

            }

            if (isOk) {
                break;
            }

        }

        if (isOk) {
            return tempMyRectFs;
        } else {
            return null;
        }

    }


    private boolean isSizeChanged = false;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        isSizeChanged = true;
    }

    private boolean mShowPic = true;

    public void setmShowDeletePic(boolean mShowPic) {
        this.mShowPic = mShowPic;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //使用id+宽高作为key，保证唯一性
        String key = getId() + File.separator + getMeasuredWidth() + File.separator + getMeasuredHeight();
        Bitmap cache = ImageCache.getInstance().getBitmapFromMemCache(key);
        if (cache == null || isSizeChanged) {//缓存背景
            Bitmap bufferBitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);//创建内存位图  
            Canvas cacheCanvas = new Canvas(bufferBitmap);//创建绘图画布  

            drawBackground(cacheCanvas);
            ImageCache.getInstance().addBitmapToMemoryCache(key, bufferBitmap);

            cache = bufferBitmap;
            isSizeChanged = false;
        }

        canvas.drawBitmap(cache, 0, 0, mPaint);
//        drawBackground(canvas);
        if (finishInflateCallBack != null) {
            finishInflateCallBack.finishInflate();
            finishInflateCallBack = null;
        }

        Item item;
        for (int i = 0; i < items.size(); i++) {
            item = items.get(i);

            item.drawRotateItem(canvas, mPaint);
        }

    }

    //横着有多少份
    private int rect_countX = 5;
    //竖着有多少份
    private int rect_countY = 6;
    private int rect_padding = 5;
    //虚线颜色
    private int dashPath_color = Color.parseColor("#808080");
    //二维数组存放所有的小区域
    private myRectF[][] rectFs = new myRectF[rect_countX + 1][rect_countY + 1];

    class myRectF {
        public RectF rectF = new RectF();
        public int x;
        public int y;
        public boolean isUse;
    }

    Paint pathPaint;

    /**
     * 画虚线
     *
     * @param c
     */
    private void drawBackground(Canvas c) {
        Path path = new Path();
        float real_height = height - rect_padding * 2;

        float left = 0;
        float top = 0;
        float oldLeft = 0;
        float oldTop = 0;
        for (int i = 0; i < rect_countX + 1; i++) {//画横着的虚线,条数为等份加一

            float y = real_height * i / rect_countX;
            top = y + rect_padding;
            if (i >= 1) {
                //将top和bottom放入数组中
                for (int j = 0; j < rect_countY; j++) {
                    if (rectFs[i - 1][j] == null) {
                        rectFs[i - 1][j] = new myRectF();
                        rectFs[i - 1][j].x = i - 1;
                        rectFs[i - 1][j].y = j;
                    }
                    rectFs[i - 1][j].rectF.top = oldTop;
                    //现在的top即底部
                    rectFs[i - 1][j].rectF.bottom = top;
                }
            }

            //记录当前的top，下次直接放入 区域数组top
            oldTop = top;

            path.reset();
            path.moveTo(rect_padding, top);
            path.lineTo(width - rect_padding, top);

            c.drawPath(path, pathPaint);
        }

        for (int j = 0; j < rect_countY + 1; j++) {
            if (rectFs[rect_countX][j] == null) {
                rectFs[rect_countX][j] = new myRectF();
                rectFs[rect_countX][j].x = rect_countX;
                rectFs[rect_countX][j].y = j;
            }

            rectFs[rect_countX][j].rectF.top = -1;
            rectFs[rect_countX][j].rectF.bottom = -1;
            rectFs[rect_countX][j].rectF.left = -1;
            rectFs[rect_countX][j].rectF.right = -1;
        }

        float real_width = width - rect_padding * 2;
        for (int i = 0; i < rect_countY + 1; i++) {//画竖着的虚线
            float x = real_width * i / rect_countY;
            left = x + rect_padding;

            if (i >= 1) {
                for (int j = 0; j < rect_countX; j++) {
                    if (rectFs[j][i - 1] == null) {
                        rectFs[j][i - 1] = new myRectF();
                        rectFs[j][i - 1].x = j;
                        rectFs[j][i - 1].y = i - 1;
                    }
                    rectFs[j][i - 1].rectF.left = oldLeft;
                    rectFs[j][i - 1].rectF.right = left;
                }
            }

            oldLeft = left;

            path.reset();
            path.moveTo(left, rect_padding + rect_padding);
            path.lineTo(left, height - rect_padding);

            c.drawPath(path, pathPaint);
        }

        for (int j = 0; j < rect_countX + 1; j++) {
            float y = real_height * j / rect_countX;
            top = y + rect_padding;

            if (rectFs[j][rect_countY] == null) {
                rectFs[j][rect_countY] = new myRectF();
                rectFs[j][rect_countY].x = j;
                rectFs[j][rect_countY].y = rect_countY;
            }
            rectFs[j][rect_countY].rectF.left = oldLeft;
            float x = real_width * (rect_countY + 1) / rect_countY;
            left = x + rect_padding;
            rectFs[j][rect_countY].rectF.right = left;
            rectFs[j][rect_countY].rectF.top = top;
            y = real_height * (j + 1) / rect_countX;
            top = y + rect_padding;
            rectFs[j][rect_countY].rectF.bottom = top;
        }
    }

    private float currentX;
    private float currentY;
    private Item currentItem;
    private boolean touchEnable = true;
    private boolean moveEnable = true;
//    private boolean isLongClick;
//    private boolean longClickEnable = true;


    public void setMoveEnable(boolean moveEnable) {
        this.moveEnable = moveEnable;
    }

    public void setTouchEnable(boolean touchEnable) {
        this.touchEnable = touchEnable;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!touchEnable) {
            return super.onTouchEvent(event);
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                currentItem = null;
//                isLongClick = false;
//                isDoubleTap = false;
//                longClickEnable = true;

                //find item
                currentX = event.getX();
                currentY = event.getY();
                currentItem = findItem();
                if (currentItem != null) {
                    currentItem.resetRectFs();
                    getParent().requestDisallowInterceptTouchEvent(true);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(false);
                    return super.onTouchEvent(event);
                }
//                if (mPreviousUpEvent != null && mCurrentDownEvent != null
//                        && isConsideredDoubleTap(mCurrentDownEvent, mPreviousUpEvent, event)) {
//                    if (mDoubleTapEnabled) {
//                        isDoubleTap = true;
//                        if (currentItem != null)
//                            currentItem.addDegree();
//                    }
//                }
//
//                mCurrentDownEvent = MotionEvent.obtain(event);

                if (mInTouchEventCount.touchCount > 2)
                    mInTouchEventCount.touchCount = 0;

                if (0 == mInTouchEventCount.touchCount) // 第一次按下时,开始统计
                    postDelayed(mInTouchEventCount, 500);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (currentItem == null)
                    break;

                float x2 = event.getX();
                float y2 = event.getY();

//                if (longClickEnable == true)
//                    isLongClick = isLongPressed(currentX, currentY, x2, y2, event.getDownTime(), event.getEventTime(), 500);

                if (mInTouchEventCount.isLongClick && moveEnable) {//长按移动
                    float scrollX = x2 - currentX;
                    float scrollY = y2 - currentY;

//                    //移动之后，下个down事件之前，不能再执行longclick
//                    longClickEnable = false;

                    currentItem.setPoint(scrollX, scrollY);
                    postInvalidate();

                    currentX = x2;
                    currentY = y2;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (currentItem != null) {
                    if (mInTouchEventCount.isLongClick && moveEnable) {//长按
                        myRectF[] newRectFs = isCorrectRegion(currentItem);
                        if (newRectFs != null) {
                            currentItem.resetRectFs();
                            currentItem.myRectFs = newRectFs;
                            currentItem.change();
                        } else {
                            currentItem.reset();
                        }

                        postInvalidate();
                    }
                    currentItem.resetRectFsT();
                    // 一次点击事件要有按下和抬起, 有抬起必有按下, 所以只需要在ACTION_UP中处理
                    mInTouchEventCount.touchCount++;
                    // 如果是长按操作, 则Handler的消息,不能将touchCount置0, 需要特殊处理
                    if (mInTouchEventCount.isLongClick) {
                        mInTouchEventCount.touchCount = 0;
                        mInTouchEventCount.isLongClick = false;
                    }

//                    if (isLongClick) {//长按
//                        myRectF[] newRectFs = isCorrectRegion(currentItem);
//                        if (newRectFs != null) {
//                            currentItem.resetRectFs();
//                            currentItem.myRectFs = newRectFs;
//                            currentItem.resetRectFsT();
//                            currentItem.change();
//                        } else {
//                            currentItem.reset();
//                        }
//
//                        postInvalidate();
//                    } else if (isDoubleTap) {//双击
//                        currentItem.resetRectFsT();
//                        postInvalidate();
//                    } else {//点击
//                        if (currentItem != null && currentItem.onItemClickLinstner != null) {
//                            if (showPic && currentItem.clickIndex == -2) {
//                                currentItem.onItemClickLinstner.onPicClick(items, currentItem);
//                                currentItem.clickIndex = -1;
//                            } else if (currentItem.clickIndex != -1) {
//                                currentItem.onItemClickLinstner.onItemClick(currentItem, currentItem.clickIndex);
//                                currentItem.clickIndex = -1;
//                            }
//                        }
//                    }
                }

//                mPreviousUpEvent = MotionEvent.obtain(event);
                break;
            }
        }

        return true;
    }

    // 统计500ms内的点击次数
    TouchEventCountThread mInTouchEventCount = new TouchEventCountThread();
    // 根据TouchEventCountThread统计到的点击次数, perform单击还是双击事件
    TouchEventHandler mTouchEventHandler = new TouchEventHandler();

    public class TouchEventHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            if (msg.arg1 == 1 || !moveEnable) {
                if (currentItem != null && currentItem.onItemClickLinstner != null) {
                    if (currentItem.isShowPic() && mShowPic && currentItem.clickIndex == -2) {
                        currentItem.onItemClickLinstner.onPicClick(items, currentItem);
                        currentItem.clickIndex = -1;
                    } else if (currentItem.clickIndex != -1) {
                        currentItem.onItemClickLinstner.onItemClick(currentItem, currentItem.clickIndex);
                        currentItem.clickIndex = -1;
                    }
                }
            } else if (msg.arg1 >= 2) {
                if (currentItem != null) {
                    currentItem.addDegree();
                    currentItem.resetRectFsT();
                    postInvalidate();
                }
            }
        }
    }

    public class TouchEventCountThread implements Runnable {
        public int touchCount = 0;
        public boolean isLongClick = false;

        @Override
        public void run() {
            Message msg = new Message();
            if (0 == touchCount) { // long click
                isLongClick = true;
            } else {
                msg.arg1 = touchCount;
                mTouchEventHandler.sendMessage(msg);
                touchCount = 0;
            }
        }
    }


    /**
     * 当前item的占用区域是否为正确的区域
     *
     * @param currentItem
     * @return
     */
    private myRectF[] isCorrectRegion(Item currentItem) {

        int weight = currentItem.weight;
        myRectF[] tempMyRectFs = new myRectF[weight];
        boolean isOk = false;
        if (currentItem.direction == 0 || currentItem.direction == 2) {
            for (int i = 0; i < rect_countX; i++) {
                for (int j = 0; j < rect_countY; j++) {
                    if (j != rect_countY - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                        for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                            if (rectFs[i][j + t] != null && rectFs[i][j + t].rectF.left != -1 && !rectFs[i][j + t].isUse) {
                                tempMyRectFs[t] = rectFs[i][j + t];

                                if (t == weight - 1) {
                                    isOk = currentItem.isInRegion(tempMyRectFs);
                                }
                            } else {
                                break;
                            }
                        }

                    } else {
                        break;
                    }

                    if (isOk) {
                        break;
                    }

                }

                if (isOk) {
                    break;
                }

            }

        } else if (currentItem.direction == 1 || currentItem.direction == 3) {
            for (int i = 0; i < rect_countY; i++) {
                for (int j = 0; j < rect_countX; j++) {
                    if (j != rect_countX - weight + 1) {//剩下的区域个数小于weight，则直接跳出循环

                        for (int t = 0; t < weight; t++) {//剩下的区域下没有连续的weight个数未用区域，则直接跳出循环
                            if (rectFs[j + t][i] != null && rectFs[j + t][i].rectF.left != -1 && !rectFs[j + t][i].isUse) {
                                tempMyRectFs[t] = rectFs[j + t][i];

                                if (t == weight - 1) {
                                    isOk = currentItem.isInRegion(tempMyRectFs);
                                }
                            } else {
                                break;
                            }
                        }

                    } else {
                        break;
                    }

                    if (isOk) {
                        break;
                    }

                }

                if (isOk) {
                    break;
                }

            }
        }

        if (isOk) {
            return tempMyRectFs;
        } else {
            return null;
        }

    }

    private final int DOUBLE_TAP_TIMEOUT = 200;
//    private MotionEvent mCurrentDownEvent;
//    private boolean mDoubleTapEnabled = true;
//    private boolean isDoubleTap = true;
//    private MotionEvent mPreviousUpEvent;

    /**
     * 是否双击
     *
     * @param firstDown
     * @param firstUp
     * @param secondDown
     * @return
     */
    private boolean isConsideredDoubleTap(MotionEvent firstDown, MotionEvent firstUp, MotionEvent secondDown) {
        if (secondDown.getEventTime() - firstUp.getEventTime() > DOUBLE_TAP_TIMEOUT) {
            return false;
        }
        int deltaX = (int) firstUp.getX() - (int) secondDown.getX();
        int deltaY = (int) firstUp.getY() - (int) secondDown.getY();
        return deltaX * deltaX + deltaY * deltaY < 10000;
    }

    //添加item时，默认位置的偏移量
    private int deviation = 5;


    private Item findItem() {
        Item item = null;
        //从后找，优先使用最上层item
        for (int i = items.size() - 1; i >= 0; i--) {
            item = items.get(i);
            //当前点击了图片
            if (item.isShowPic() && mShowPic) {
                if (item.direction == 1) {
                    if (currentX < item.myRectFs[item.weight - 1].
                            rectF.right + item.myRectFs[item.weight - 1].rectF.width() / 3 && currentX > item.myRectFs[item.weight - 1].rectF.right - item.myRectFs[item.weight - 1].rectF.width() / 3
                            && currentY > item.myRectFs[0].rectF.top - item.myRectFs[0].rectF.height() / 3 && currentY < item.myRectFs[0].rectF.top + item.myRectFs[0].rectF.height() / 3) {
                        item.clickIndex = -2;
                        break;
                    }
                } else if (item.direction == 2) {
                    if (currentX < item.myRectFs[0].
                            rectF.right + item.myRectFs[0].rectF.width() / 3 && currentX > item.myRectFs[0].rectF.right - item.myRectFs[0].rectF.width() / 3
                            && currentY > item.myRectFs[0].rectF.top - item.myRectFs[0].rectF.height() / 3 && currentY < item.myRectFs[0].rectF.top + item.myRectFs[0].rectF.height() / 3) {
                        item.clickIndex = -2;
                        break;
                    }
                } else {
                    if (currentX < item.myRectFs[item.weight - 1].rectF.right + item.myRectFs[item.weight - 1].rectF.width() / 3 && currentX > item.myRectFs[item.weight - 1].rectF.right - item.myRectFs[item.weight - 1].rectF.width() / 3
                            && currentY > item.myRectFs[item.weight - 1].rectF.top - item.myRectFs[item.weight - 1].rectF.height() / 3 && currentY < item.myRectFs[item.weight - 1].rectF.top + item.myRectFs[item.weight - 1].rectF.height() / 3) {
                        item.clickIndex = -2;
                        break;
                    }
                }
            }

            if (currentX < item.right + deviation && currentX > item.left - deviation
                    && currentY > item.getTop() - deviation && currentY < item.getBottom() + deviation) {

                for (int j = 0; j < item.myRectFs.length; j++) {//获取到当前点击的小区域索引
                    if (currentX < item.myRectFs[j].rectF.right + deviation && currentX > item.myRectFs[j].rectF.left - deviation
                            && currentY > item.myRectFs[j].rectF.top - deviation && currentY < item.myRectFs[j].rectF.bottom + deviation) {
                        item.clickIndex = j;
                        break;
                    }
                }
                break;
            } else {
                item = null;
            }
        }

        return item;
    }


    /**
     * 计算两点与垂直方向夹角
     *
     * @param p1
     * @param p2
     * @return
     */
    public float computeDegree(Point p1, Point p2) {
        float tran_x = p1.x - p2.x;
        float tran_y = p1.y - p2.y;
        float degree = 0.0f;
        float angle = (float) (Math.asin(tran_x / Math.sqrt(tran_x * tran_x + tran_y * tran_y)) * 180 / Math.PI);
        if (!Float.isNaN(angle)) {
            if (tran_x >= 0 && tran_y <= 0) {//第一象限    
                degree = angle;
            } else if (tran_x <= 0 && tran_y <= 0) {//第二象限    
                degree = angle;
            } else if (tran_x <= 0 && tran_y >= 0) {//第三象限    
                degree = -180 - angle;
            } else if (tran_x >= 0 && tran_y >= 0) {//第四象限    
                degree = 180 - angle;
            }
        }
        return degree;
    }

    public class Item {
        private int resId;
        private int oldResId;
        private int deleteResId;
        private Object data;
        private int weight = 1;
        private float centerX;
        private float centerY;
        private float left;
        private float top;
        private float right;
        private float bottom;
        private float deta_degree;
        private float width;
        private float height;
        private RectF rectF;
        private myRectF[] myRectFs;
        private int padding = 10;
        //放入的偏移量
        private int in_padding = 20;
        // 0 :朝下 1：朝左 2：朝上 3 ： 朝右
        private int direction = 0;
        private OnItemClickLinstner onItemClickLinstner;
        //默认-1 ， -2：點了圖片
        private int clickIndex = -1;
        //显示删除图片
        private boolean showPic = true;
//        private Paint tempPaint;

        public Item(int resId, Object data, myRectF[] addMyRectFs, int weight) {
            this.resId = resId;
            this.data = data;
            this.myRectFs = addMyRectFs;
            this.weight = weight;
            this.rectF = new RectF(addMyRectFs[0].rectF.left, addMyRectFs[0].rectF.top,
                    addMyRectFs[weight - 1].rectF.right, addMyRectFs[weight - 1].rectF.bottom);
            //缓存机制
            Bitmap bitmap = getBitmap(this.resId, rectF, direction, padding);
            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;

            width = bitmap.getWidth();
            height = bitmap.getHeight();

            centerX = rectF.centerX();
            centerY = rectF.centerY();

//            tempPaint = new Paint();
//            tempPaint.setColor(Color.RED);
        }

        public void resetRectFs() {
            for (int i = 0; i < myRectFs.length; i++) {
                myRectFs[i].isUse = false;
            }
        }

        public void resetRectFsT() {
            for (int i = 0; i < myRectFs.length; i++) {
                myRectFs[i].isUse = true;
            }
        }

        public void change() {
            this.rectF = new RectF(myRectFs[0].rectF.left, myRectFs[0].rectF.top,
                    myRectFs[weight - 1].rectF.right, myRectFs[weight - 1].rectF.bottom);

            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;
            centerX = rectF.centerX();
            centerY = rectF.centerY();

            isMove = true;
        }

        public void reset() {

            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;
            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }

        public void resetByRotate() {
            this.rectF = genernateRectFByRotate(myRectFs);

            left = rectF.left;
            right = rectF.right;
            top = rectF.top;
            bottom = rectF.bottom;
            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }

        private RectF genernateRectFByRotate(myRectF[] tempMyRectFs) {
            RectF rectF = null;
            if (direction == 0 || direction == 1) {
                rectF = new RectF(myRectFs[0].rectF.left, myRectFs[0].rectF.top,
                        myRectFs[weight - 1].rectF.right, myRectFs[weight - 1].rectF.bottom);
            } else if (direction == 2 || direction == 3) {
                rectF = new RectF(myRectFs[weight - 1].rectF.left, myRectFs[weight - 1].rectF.top,
                        myRectFs[0].rectF.right, myRectFs[0].rectF.bottom);
            }

            return rectF;
        }

        public boolean isInRegion(myRectF[] tempMyRectFs) {
            if (left > tempMyRectFs[0].rectF.left - in_padding
                    && right < tempMyRectFs[weight - 1].rectF.right + in_padding
                    && top > tempMyRectFs[0].rectF.top - in_padding
                    && bottom < tempMyRectFs[weight - 1].rectF.bottom + in_padding) {
                return true;
            }

            return false;
        }

        public boolean isShowPic() {
            return showPic;
        }

        public void setShowPic(boolean showPic) {
            this.showPic = showPic;
        }

        public int getDeleteResId() {
            return deleteResId;
        }

        public void setDeleteResId(int deleteResId) {
            this.deleteResId = deleteResId;
        }

        public Object getData() {
            return data;
        }

        public float getLeft() {
            return left;
        }

        public void setLeft(float left) {
            this.left = left;
        }

        public float getTop() {
            return top;
        }

        public void setTop(float top) {
            this.top = top;
        }

        public float getRight() {
            return right;
        }

        public void setRight(float right) {
            this.right = right;
        }

        public float getBottom() {
            return bottom;
        }

        public void setBottom(float bottom) {
            this.bottom = bottom;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public int getDirection() {
            if (weight == 1) {
                if (deta_degree == 90) {//顺时针转90
                    return 1;
                } else if (deta_degree == 180) {
                    return 2;
                } else if (deta_degree == 270) {
                    return 3;
                } else if (deta_degree == 360 || deta_degree == 0) {
                    return 0;
                }
            }
            return direction;
        }

        public void drawRotate(Canvas canvas, Bitmap rotateBitmap, Paint paint) {
            if (rotateBitmap == null) {
                return;
            }
            canvas.drawBitmap(rotateBitmap, right - rotateBitmap.getWidth(), top, paint);
        }

        public void drawItem(Canvas canvas, Paint paint) {
            canvas.drawBitmap(getBitmap(this.resId, rectF, direction, padding), left, top, paint);
        }

        public void drawRotateItem(Canvas canvas, Paint paint) {
//            canvas.drawOval(rectF, tempPaint);
            Matrix matrix = new Matrix();
            matrix.postRotate(deta_degree, width / 2, height / 2);
            if (direction == 0 || direction == 2) {
                matrix.postTranslate(left + padding / 2, top + padding / 2);
            } else if (direction == 1 || direction == 3) {
                matrix.postTranslate(left - rectF.height() / (2 * weight) + padding / 2, top + rectF.height() / (weight * 2) + padding / 2);
            }

            canvas.drawBitmap(getBitmap(this.resId, rectF, direction, padding), matrix, paint);

            if (isShowPic() && mShowPic)
                drawRotate(canvas, getDeleteBitmap(deleteResId), paint);
        }

        private float oldDegree;

        /**
         * 通过此方法来控制旋转度数，如果超过360，让它求余，防止，该值过大造成越界
         *
         * @param added
         */
        private void addDegree(float added) {
            if (oldDegree == 0) {
                oldDegree = added;
            }

            deta_degree += added - oldDegree;
            if (deta_degree > 360 || deta_degree < -360) {
                deta_degree = deta_degree % 360;
            }

            oldDegree = added;
        }

        private boolean isMove;

        protected void setDirection(int direction) {
            if (direction < 0 || direction > 3) {
                return;
            }

//            resetRectFs();
            int addCount = direction - this.direction;
            for (int i = 0; i < addCount; i++) {
                addDegreeNoException();
            }

            resetByRotate();
//            resetRectFsT();
//            deta_degree += addCount * 90 - 90;
//
//            addDegree();
        }

        public int getX() {
            myRectF tempRectF;
            tempRectF = myRectFs[0];
            if (isMove) {
                if ((direction == 3 || direction == 2))
                    tempRectF = myRectFs[weight - 1];
            }

            if (weight == 2 && direction == 3) {
                return tempRectF.x - 1;
            }
            //适配ios

            return tempRectF.x;
        }

        public int getY() {
            myRectF tempRectF;
            tempRectF = myRectFs[0];
            if (isMove) {
                if ((direction == 3 || direction == 2))
                    tempRectF = myRectFs[weight - 1];
            }

            //适配ios
            if (weight == 2 && direction == 2) {
                return tempRectF.y - 1;
            }


            return tempRectF.y;
        }

        /**
         * 每次+90度
         */
        private void addDegreeNoException() {
            deta_degree += 90;
            if (deta_degree > 360 || deta_degree < -360) {
                deta_degree = deta_degree % 360;
            }

            if (weight == 1) {
                return;
            }

            if (deta_degree == 90) {//顺时针转90
                direction = 1;
            } else if (deta_degree == 180) {
                direction = 2;
            } else if (deta_degree == 270) {
                direction = 3;
            } else if (deta_degree == 360 || deta_degree == 0) {
                direction = 0;
            }

            myRectF[] tempRectFs = new myRectF[weight];
            tempRectFs[0] = myRectFs[0];
            if (isMove) {
                if ((direction == 3 || direction == 0))
                    tempRectFs[0] = myRectFs[weight - 1];
            }

            for (int i = 1; i < myRectFs.length; i++) {//第一个元素不变
                if (direction == 1) {//顺时针转90,x变为第一个元素x+i，y变为第一个元素y
                    tempRectFs[i] = rectFs[tempRectFs[0].x + i][tempRectFs[0].y];
                } else if (direction == 2) {//顺时针转90,x变为第一个元素x，y变为第一个元素y-i
                    tempRectFs[i] = rectFs[tempRectFs[0].x][tempRectFs[0].y - i];
                } else if (direction == 3) {//顺时针转90,x变为第一个元素x -i，y变为第一个元素y
                    tempRectFs[i] = rectFs[tempRectFs[0].x - i][tempRectFs[0].y];
                } else if (direction == 0) {//顺时针转90,x变为第一个元素x，y变为第一个元素y+i
                    tempRectFs[i] = rectFs[tempRectFs[0].x][tempRectFs[0].y + i];
                }
            }

            myRectFs = tempRectFs;
            isMove = false;
        }

        /**
         * 每次+90度
         */
        private void addDegree() {
            deta_degree += 90;
            if (deta_degree > 360 || deta_degree < -360) {
                deta_degree = deta_degree % 360;
            }

            if (weight == 1) {
                return;
            }

            if (deta_degree == 90) {//顺时针转90
                direction = 1;
            } else if (deta_degree == 180) {
                direction = 2;
            } else if (deta_degree == 270) {
                direction = 3;
            } else if (deta_degree == 360 || deta_degree == 0) {
                direction = 0;
            }

            myRectF[] tempRectFs = new myRectF[weight];
            tempRectFs[0] = myRectFs[0];
            if (isMove) {
                if ((direction == 3 || direction == 0))
                    tempRectFs[0] = myRectFs[weight - 1];
            }
            List<myRectF> tempMyRectFs = new ArrayList<>(weight);

            try {
                for (int i = 1; i < myRectFs.length; i++) {//第一个元素不变
                    if (direction == 1) {//顺时针转90,x变为第一个元素x+i，y变为第一个元素y
                        tempRectFs[i] = rectFs[tempRectFs[0].x + i][tempRectFs[0].y];
                        if (tempRectFs[i].isUse || tempRectFs[i].rectF.left == -1) {
                            throw new Exception();
                        }
                        tempMyRectFs.add(tempRectFs[i]);
                        tempRectFs[i].isUse = true;
                    } else if (direction == 2) {//顺时针转90,x变为第一个元素x，y变为第一个元素y-i
                        tempRectFs[i] = rectFs[tempRectFs[0].x][tempRectFs[0].y - i];
                        if (tempRectFs[i].isUse || tempRectFs[i].rectF.left == -1) {
                            throw new Exception();
                        }
                        tempMyRectFs.add(tempRectFs[i]);
                        tempRectFs[i].isUse = true;
                    } else if (direction == 3) {//顺时针转90,x变为第一个元素x -i，y变为第一个元素y
                        tempRectFs[i] = rectFs[tempRectFs[0].x - i][tempRectFs[0].y];
                        if (tempRectFs[i].isUse || tempRectFs[i].rectF.left == -1) {
                            throw new Exception();
                        }
                        tempMyRectFs.add(tempRectFs[i]);
                        tempRectFs[i].isUse = true;
                    } else if (direction == 0) {//顺时针转90,x变为第一个元素x，y变为第一个元素y+i
                        tempRectFs[i] = rectFs[tempRectFs[0].x][tempRectFs[0].y + i];
                        if (tempRectFs[i].isUse || tempRectFs[i].rectF.left == -1) {
                            throw new Exception();
                        }
                        tempMyRectFs.add(tempRectFs[i]);
                        tempRectFs[i].isUse = true;
                    }
                }

                resetRectFs();
                myRectFs = tempRectFs;
                resetByRotate();
                isMove = false;
            } catch (Exception e) {
                e.printStackTrace();
                for (myRectF myRectF : tempMyRectFs) {
                    if (myRectF != null) {
                        myRectF.isUse = false;
                    }
                }
                //回滚
                deta_degree -= 90;
                direction--;
                if (direction < 0) {
                    direction = 3;
                }
                if (deta_degree > 360 || deta_degree < -360) {
                    deta_degree = deta_degree % 360;
                }
            }

        }

        public void setPoint(float scrollX, float scrollY) {
            left += scrollX;
            right += scrollX;
            top += scrollY;
            bottom += scrollY;

            centerX = rectF.centerX();
            centerY = rectF.centerY();
        }

        public void setResId(int resId) {
            if (resId == this.resId) {
                return;
            }
            oldResId = this.resId;
            this.resId = resId;
            postInvalidate();
        }

        public void rollBackResId() {
            if (oldResId == 0)
                return;
            this.resId = oldResId;
            postInvalidate();
        }
    }

    public Bitmap getBitmap(int resId, RectF rectF, int direction, int padding) {
        if (context == null) {
            return null;
        }

        String key;
        if (direction == 1 || direction == 3) {//竖着的时候宽高反转
            key = resId + File.separator + (int) rectF.height() + File.separator + (int) rectF.width();//保证key的唯一性，防止布局宽高变化后获取的bitmap宽高不适应
        } else {
            key = resId + File.separator + (int) rectF.width() + File.separator + (int) rectF.height();//保证key的唯一性，防止布局宽高变化后获取的bitmap宽高不适应
        }
        //缓存机制中取
        if (ImageCache.getInstance().getBitmapFromMemCache(String.valueOf(key)) == null) {
            //缓存机制
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resId, new BitmapFactory.Options());
            Matrix matrix = new Matrix();
            if (direction == 1 || direction == 3) {
                matrix.postScale((rectF.height() - padding) / (float) bitmap.getWidth(), (rectF.width() - padding) / (float) bitmap.getHeight());
            } else {
                matrix.postScale((rectF.width() - padding) / (float) bitmap.getWidth(), (rectF.height() - padding) / (float) bitmap.getHeight());
            }
            //长和宽放大缩小的比例
            Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            ImageCache.getInstance().addBitmapToMemoryCache(String.valueOf(key),
                    resizeBmp);
            bitmap.recycle();
        }

        return ImageCache.getInstance().getBitmapFromMemCache(String.valueOf(key));
    }

    public interface OnItemClickLinstner {
        public void onItemClick(Item item, int index);

        public void onPicClick(List<Item> items, Item item);
    }
}
